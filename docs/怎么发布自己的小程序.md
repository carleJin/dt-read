# 怎么发布自己的小程序

> 使用的工具如果没有安装，请自行百度安装

1. **下载代码到本地**
   ```shell
   # 默认已安装Git工具
   git clone git@gitee.com:wtto00/dt-read.git
   ```
1. **安装依赖**
   ```shell
   # 默认已安装nodejs环境，且已安装yarn工具
   cd dt-read
   yarn
   ```
1. **申请一个小程序**  
   已有小程序的，可以省略此步骤  
   [腾讯官网申请注册一个小程序](https://mp.weixin.qq.com/wxopen/waregister?action=step1&token=&lang=zh_CN)
1. **修改 appid**  
   到[小程序后台](https://mp.weixin.qq.com/wxamp/devprofile/get_profile?token=930514308&lang=zh_CN)，依次点击 `开发`>`开发设置`，可以看到自己小程序的`AppID(小程序ID)`，复制此字符串，打开代码文件`/src/manifest.json`，找到内容`"mp-weixin":{"appid":"wx58311362dbfc378d"}`，把自己小程序的 appid 覆盖此处的 appid。
1. **安装打开微信开发者工具**  
   已安装的可忽略  
   到[腾讯官网下载开发者工具](https://developers.weixin.qq.com/miniprogram/dev/devtools/download.html)，安装完成后，打开
1. **生成小程序发布包**
   ```
   yarn build:mp-weixin
   ```
1. **复制小程序云函数**
   ```shell
   # windows系统cmd以及powershell不识别命令，可以使用git bash
   sh function.sh build
   ```
   之后在开发者工具编辑器里面，上传云函数（即右键目录`cloudfunctions`,点击上传部署即可）
1. **开发者工具导入小程序**  
   打开微信开发者工具，点击`小程序-导入项目`，点击`目录`，选择目录`/dist/build/mp-weixin`，然后点击右下角`导入`按钮
1. **小程序云开发数据库**  
   微信开发者工具导入项目后，点击微信开发者工具上面的`云开发`按钮，首次进入需要申请一个免费的空间即可，然后点击`云开发控制台`窗口上方的`数据库`按钮，点击左侧上方`集合名称`旁边的`+`按钮添加集合。集合名称依次按照顶部截图中的创建。创建 7 个集合后，点击集合`book_sources`，然后点击右侧的导入，选择文件`/src/static/database_export.json`
1. **云函数绑定空间**  
   在微信开发者工具编辑器左侧的资源管理器中，右键目录`cloudfunctions`，选择当前环境为自己申请的免费空间。  
   然后右键每一个云函数，选择上传并部署，如下图所示：
   ![上传部署云函数](https://images.gitee.com/uploads/images/2020/0614/133836_03aacaa9_2352095.jpeg '微信图片编辑_20200614133355.jpg')
   之后可以在云开发控制台云函数中看到上传的云函数列表，如下图所示：
   ![云函数控制台](https://images.gitee.com/uploads/images/2020/0614/133920_91cdbfbe_2352095.png '微信截图_20200614133522.png')
1. **测试**  
   你现在可以在微信开发者工具左侧的预览模式中体验一下，是否可以正常工作了
1. **发布**  
   点击微信开发者工具上方的按钮`上传`，就可以发布一个体验版本了。版本管理可以在小程序后台-管理-版本管理，中查看。
