// 云函数入口文件
const cloud = require("wx-server-sdk");

cloud.init();

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext();

  cloud.updateConfig({
    env: wxContext.ENV
  });

  const db = cloud.database();

  const data = {
    openid: wxContext.OPENID
  };

  const res = await db
    .collection("config")
    .where({
      _openid: wxContext.OPENID
    })
    .get();

  if (res.data && res.data.length > 0) {
    data.config = res.data[0];
  } else {
    const cres = await db.collection("config").add({
      data: {
        _openid: wxContext.OPENID,
      }
    });
    db.collection('users').add({
      data: {
        _id: wxContext.OPENID,
      }
    })
    if (cres._id) {
      data.config = { _id: cres._id };
    }
  }

  return { errMsg: "ok", data };
};
